from rlcompleter import readline
from ssl import AlertDescription
readline.parse_and_bind("tab:complete")

# Imports for imoose
from imoose.solver import *
from imoose.kernel import *
from imoose.solverutils      import *
from imoose.solver import pysolvers
from imoose.solverutils.materialutils.materialproperties_vector import MaterialProperty as MaterialProperty_Matrix
from imoose.solverutils.materialutils.materialproperties_utils import HRemanenz
from imoose.solverutils.materialutils.materialproperties_scalar import MaterialProperty
from imoose.solverutils.NewtonForHysterese import Newton
from imoose.solverutils.materialutils.materialproperties_utils import BrFieldExcitation
import numpy as np
import os
from imoose.solverutils.materialutils.materialproperties_utils import currentFromFile




# PARAMETERS ##################################
# Define some variables
meshName = "Team32in3D"                        # filename of the mesh




# Variables definition for the model 

scale    = 1.0                             # scaling factor of the mesh
order = 1                   # order of the solution
length = 1                  # depth of the model
theta=1.0
f=10
periode = 2
phase = 0

deltaStep=50
#deltaStep=125 # Simuliert jeden 10. Schritt
turns=90.   #turns of one coil
maxStep=1001*periode

deltaT=deltaStep*(1.0/(maxStep*f))*periode   #delta T . 

Jr=0 #set crossing of y-Axis of BH plot

#Eingeben
#########################################################################################
soluName =  '1_A_solver_deltaStep_'+str(deltaStep)           # filename of the solution #
case = 4                                                                               #
#########################################################################################


Hysteresis_Material = {"TeamWork 32": 1, "M330": 0, "M400": 0}
if Hysteresis_Material["TeamWork 32"]==1:
    path_anhys = '/home/xiao/home/unixhome/eclipse-workspace/Team32_EPNC2022/materials/Algebraisch_anhys_surface_BH_nud_indexinterp.mat'
    path_R = '/home/xiao/home/unixhome/eclipse-workspace/Team32_EPNC2022/materials/threshold_surf_Team32_fein107_v1_indexinterp.mat'

    StopParameter =  np.array([Hysteresis_Material,{'gewichts_N3':np.array([1.,1.26150721986240,9.61002096449036])},\
                           {'kx':0,'ky':0},{'N':3},{'Bx_max':1.8,'By_max':1.8},{'verteilung_N3': np.array([0.977188479567984,0.444144997898313])},\
                           {'pathHBNu':path_anhys},{'pathR':path_R}])  
elif Hysteresis_Material["M330"]==1:
    path_anhys = '/home/xiao/home/unixhome/3_Fertiges_Modell/1_data_for_simulation/STOP_Modell/M330/anhysSurfIndexM330500.mat'
    path_R = '/home/xiao/home/unixhome/3_Fertiges_Modell/1_data_for_simulation/STOP_Modell/M330/thresholdSurfIndexM330500.mat'
    StopParameter =  np.array([Hysteresis_Material,\
                               {'gewichts_N2':np.array([1.,0.497837109437846]),\
                                'gewichts_N3':np.array([1.,0.094948261551696,0.596307291137001]),\
                                'gewichts_N4':np.array([1.,0.033386627307850,0.102136652818399,0.571801259287212]),\
                                'gewichts_N5':np.array([1.,0.032295293396905,0.086140796234127,0.643192079587799,0.011040180636233]),\
                                'gewichts_N6':np.array([1.,0.0319070334070424,0.0862932888310559,0.402574035703909,0.285932673847960,0.248644571198072]),\
                                'gewichts_N7':np.array([1.,0.0326114308455136,0.0877316287728461,0.0999430432351180,0.493090916650583,0.0627312436826254,0.00181049048173554]),\
                                'gewichts_N8':np.array([1.,0.0332446177934657,0.0488307220895686,0.334041253334485,1.01157086262367,0.568082254622412,0.0609283191829594,0.0906449066769699])},\
                               {'kx':0,'ky':0},{'N':4},{'Bx_max':1.8,'By_max':1.8},\
                               {'verteilung_N2': np.array([0.791792404412608]),\
                                'verteilung_N3': np.array([1.962211765679026,0.441801791528829]),\
                                'verteilung_N4': np.array([4.478802908419705,1.294476463385386,0.412453907743588]),\
                                'verteilung_N5': np.array([4.47876036270930, 1.43172367430289,0.377931650201727,0.255039424994660]),\
                                'verteilung_N6': np.array([4.47860684339830,1.43214262211965,0.428791004447238,0.222551753549611,0.0291651216337858]),\
                                'verteilung_N7': np.array([4.47936772277537,1.43040484947457,0.491957823731060,0.348352653213209,0.390859961544786,0.00262252605565454]),\
                                'verteilung_N8': np.array([4.47728919145912,1.00494991594967,0.491233279387937,0.00373619067225436,0.103962612599948,1.43208741138910,0.00868419251873367])},\
                               {'pathHBNu':path_anhys},{'pathR':path_R}])

elif Hysteresis_Material["M400"]==1:
    path_anhys = '/home/xiao/home/unixhome/3_Fertiges_Modell/1_data_for_simulation/STOP_Modell/M400/anhysSurfIndexM400500.mat'
    path_R = '/home/xiao/home/unixhome/3_Fertiges_Modell/1_data_for_simulation/STOP_Modell/M400/threshold_surf_M400_index_500_v2.mat'
    StopParameter =  np.array([Hysteresis_Material,\
                               {'gewichts_N2':np.array([1.,0.388809114309877]),\
                                'gewichts_N3':np.array([1.,0.092807205560258,0.518360660145255]),\
                                'gewichts_N4':np.array([1.,0.0824839860134596,0.0952562097227808,0.436910927315896]),\
                                'gewichts_N5':np.array([1.,0.105138227920776,0.0925380511925895,0.465643800745378,0.0784437504857486])},\
                               {'kx':0,'ky':0},{'N':3},{'Bx_max':1.8,'By_max':1.8},\
                               {'verteilung_N2': np.array([1.63929294535062]),\
                                'verteilung_N3': np.array([3.552981326259392,0.678378674751005]),\
                                'verteilung_N4': np.array([3.71426768016695,1.18670533260615,0.570865562524389]),\
                                'verteilung_N5': np.array([3.32179592285588,0.490589716863991,0.517930114853202,0.432850243885263])},\
                               {'pathHBNu':path_anhys},{'pathR':path_R}])





if case==1: #1 is sinussoidal in both coils
    soludir = os.getcwd()+'/solution/'+soluName+'_case1'
    cur=currentFromFile('currents/case_1/case1a_i.dat')
    # points have to be changed according to the new geometrie
    x1 = 0.015; y1 = 0.0875   # C5
    x2 = 0.08725 ; y2 = 0.0875 # C6
    z=0
    P1=pygeo.GeoVec(x1, y1, z)
    P2=pygeo.GeoVec(x2, y2, z)
    currentx = []
    currenty = []
    for i in range (0,periode):
        currentx= currentx + cur.currentValue1
        currenty= currenty + cur.currentValue2
elif case==2:
    soludir = os.getcwd()+'/solution/'+soluName+'_case2'
    cur=currentFromFile('currents/case_2/case2_i.dat')
    # points have to be changed according to the new geometrie
    x1 = 0.015; y1 = 0.0875   # C5
    x2 = 0.08725 ; y2 = 0.0875 # C6
    z=0
    P1=pygeo.GeoVec(x1, y1, z)
    P2=pygeo.GeoVec(x2, y2, z)
    currentx = []
    currenty = []
    for i in range (0,periode):
        currentx= currentx + cur.currentValue1
        currenty= currenty + cur.currentValue2
   
   
elif case==3: # 3 is sinus and cosinus
    soludir = os.getcwd()+'/solution/'+soluName+'_case3'
    cur=currentFromFile('currents/case_3/case3_i.dat')
    # points have to be changed according to the new geometrie
    #2 ===========================================================================
    x1 = 0.08725; y1 = 0.15   # C3C4
    x2 = 0.08725 ; y2 = 0.16 # C1C2
    #===========================================================================
    z=0
    P1=pygeo.GeoVec(x1, y1, z)
    P2=pygeo.GeoVec(x2, y2, z)
    currentx = []
    currenty = []
    for i in range (0,periode):
        currentx= currentx + cur.currentValue1
        currenty= currenty + cur.currentValue2
    #currentx=cur.currentValue1    #current in the coils in x direction
    #currenty=cur.currentValue2    #current in the coils in y direction
elif case==4:
    soludir = os.getcwd()+'/solution/'+soluName+'_case4'
    cur=currentFromFile('currents/case_4/case4_i.dat')
        # points have to be changed according to the new geometrie
    x1 = 0.08725 ; y1 = 0.0875  # C6
    x2 = 0.1595; y2 = 0.0875  # C7
    z=0
    P1=pygeo.GeoVec(x1, y1, z)
    P2=pygeo.GeoVec(x2, y2, z)
    currentx = []
    currenty = []
    current1 = [i * 1 for i in cur.currentValue1]
    current2 = [i * 1 for i in cur.currentValue2]
    for i in range (0,periode):
        currentx= currentx + current1
        currenty= currenty + current2
    #currentx = []
    #currenty = []
    #for i in range (0,periode):
    #    currentx= currentx + cur.currentValue1*4
    #    currenty= currenty + cur.currentValue2*4
    #currentx=cur.currentValue1    #current in the coils in x direction
    #currenty=cur.currentValue2    #current in the coils in y direction





class MagnetoStatic3D(pystatram3d.statram3dPetsc):
        """Specialisation of PETSc based non-linear magnetostatic 3D-solver.
        a-formulation.
        """
    # Constructor
        def __init__(self, mesh, boundarylist, solver, preconditioner, order):
                pystatram3d.statram3dPetsc.__init__(
                       self, mesh, boundarylist, solver, preconditioner)
                self.__mesh        = mesh
                self.__order       = order
                self.__fluxdensity = pypost.PostEdgeDouble(mesh)

    # Add method to solver for preparation of the material properties
        def prepareMaterialProperties(self,Nullsolu=None):
            pe = pypost.PostEdgeDouble(self.__mesh)
            # Write fem solution of A into post solution
            pe.fromFemSolu(self.getEdgeSolution(), self.__order)
            # Calculate B via B = curl(A)
            self.__fluxdensity = pe.curl()
            # Set material parameters with respekt to B
            self.nu.setResultAttr(self.__fluxdensity)
            self.nu_matrix.setResultAttr(self.__fluxdensity)
        def prepareMaterialPropertiesTimeStep(self,Nullsolu =None): #, oldSoluA
            self.__fluxdensity = self.getFluxdensity(self.getEdgeSolution())
            self.nu_matrix.setResultAttr(self.__fluxdensity)
            self.nu.setResultAttr(self.__fluxdensity)  
            self.nu_matrix.setResultAttrTime()
        def getFluxdensity(self, solu=None):
            _pe = pypost.PostEdgeDouble(self.__mesh)
            _fluxdensitype = pypost.PostEdgeDouble(self.__mesh)
            if solu==None:
                _pe.fromFemSolu(self.getEdgeSolution(), self.__order)
            else: 
                _pe.fromFemSolu(solu, self.__order)
            _fluxdensity = _pe.curl()
            return _fluxdensity

# Interface #####################
# Method to calculate the electric vectorpotential T of each phase
def generic_excitationcurrent(windingconfig):
    windingCurrentMap = {}
    
    # Loop over phases
    for phase, phaseWinding in windingconfig.items():
        sourceCurrentMap = pyedgesolver.SourceCurrentMapDouble()
        coilLabels = []
        
        # Loop over each coil of the Phase
        for coilLabel, coilConfig in phaseWinding.items():
            coilLabels.append(coilLabel)
            
            #Loop over each node label of the coil with a defined current
            for nodeLabel, currentVector in coilConfig.items():
                if isinstance(currentVector, float):
                   sourceCurrentMap[nodeLabel] = pyedgesolver.CurrentPairDouble(currentVector,
                                                                                pygeo.GeoVec(0., 0., 0.))
                else:
                    turns = abs(currentVector)
                    sourceCurrentMap[nodeLabel] = pyedgesolver.CurrentPairDouble(turns, 
                                                                                 currentVector.norm())
        
        # Create fem-solver for T calculation
        tProb = pyedgesolver.EdgeTPetscDouble()
        
        # Feed solver with needet informations
        tProb.setup(fm, tuple(coilLabels), sourceCurrentMap, cu, (), ())
        
        # Calculate the solution
        tProb.solve()
        windingCurrentMap[phase] = (pypost.EdgeSoluDouble(tProb.getSolution()), tuple(coilLabels))
        
        # Prepare to write current density
        pe = pypost.PostEdgeDouble(fm.getSubMesh(coilLabels, soluName + '_' + str(phase)))
        pe.fromFemSolu(windingCurrentMap[phase][0], order)
        # Write current density to disk
        # J = rot(T)
        sfh.writeSoluMidValue(pe.getFemMeshPointer().getName() + '_J', pe.curl())
    return windingCurrentMap
#petscinterface = PetscInterface(['dummy','-ksp_monitor_draw', '-ksp_monitor'])
#petscinterface = PetscInterface(['dummy','-ksp_monitor'])
#petscinterface = PetscInterface(['dummy', '-ksp_initial_guess_nonzero', '-ksp_divtol', '1e50'])
D3=512
petscinterface = pysolvers.PetscInterface([b'', b'', b'-ksp_divtol', b'1e50'])
# create filehandler strategies and filehandlers ###########################
s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_BASIC)
fh  = pyfilehandlers.FileHandler('Ansys60', s, True, D3)
fm  = fh.readMesh('./model/'+meshName, order, scale, pyelem.MX)#MX,ED?
sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', True)
cu  = pymaterial.MatPropConstDouble(4.73e5)


# initialize zeros #####
## A from solver
ns = pypost.NodeSoluDouble() # initial A Solution

## A for post
ps = pypost.PostScalDouble(fm)
ps.pushConst(0.0)
## B output
pv = pypost.PostVecDouble(fm)
pv.pushZvec(ps)
pv.curl()

unitz = pygeo.GeoVec(0.0, 0.0, 0.0)
# Excitation quantities, fill with default vector (zero)
vZero = {}
for i in fm.allLabels():
        vZero[i] = unitz
zeroField = pystatic.ExciteLabelGeoVec(vZero)
##################### Exitations #######################
# current soucre
j_left=np.asarray(currentx)* turns#/(fm.volumeOfRegion(1)) * turns
j_right=np.asarray(currenty)* turns#/(fm.volumeOfRegion(3)) * turns
j = pystatic.ExciteLabelGeoVec({
    1: pygeo.GeoVec(0.0, 0.0, 0.0),
    2: pygeo.GeoVec(0.0, 0.0, 0.0),
    3: unitz,
    4: unitz,
    5: unitz,
    6: unitz,
    })
#remanence
bDict = {}
for i in fm.allLabels(): 
               bDict[i] = unitz
br = BrFieldExcitation(fm,bDict)##{1:pygeo.GeoVec(0.0, Jr, 0.0)})
# coercivity
hr = HRemanenz()

#################### Material Props ####################
  
# conductivity
psCon = pypost.PostScalDouble(fm)
psCon.pushConst(0.0)
nuDictCon = {}
for i in fm.allLabels(): nuDictCon[i] = './material/luft.conduct'
eddy=() #Labels for solver == Labels in Dict
conductivity = pymaterial.MatPropDepLabel(nuDictCon, psCon)

### set reluctivity labels

# general
materials={
    1: 'material/luft.inds_relu',
    2: 'hyst',
    3: 'material/luft.inds_relu',
    4: 'material/luft.inds_relu'
    }  

# scalar
linear=(1,3,4)
nonlinear=None
direction=None
frequency=None
frequency_set={}
cutedge=None
stress=None
pm=None


# tensor
aniso=None #()
aniso_dir={} #{1: 90.,} # Label: angle(deg) 0,90 save
pmmag=None#()
pmmag_dir={} # Label: x=1 y=2 aniso direction
hyst=(2,)
hyst_mod={"PAM":0, "STOP":1, "PLAY":0}

dimension=3
# femprob and main loop
tensorlabels=(2,) #None is not allowed, has to be list or tuple

reluctivity = MaterialProperty(fm,materials,linear,nonlinear)
reluctivity_matrix=MaterialProperty_Matrix(fm,materials,aniso,aniso_dir,pmmag,pmmag_dir,hyst,hyst_mod,StopParameter,dimension)
reluctivity_matrix.setdeltaT(deltaT)


################### Initialize Femprob ###################
## changes if movement is applied boundaries change and therefore femprob is created for each movement step !!!
boundaries = pyfem.BoundaryListBuilderEdgeIdDouble(fm, (), (), {
    101: 0.0,  # nodeLabel: value
    102: 0.0,
    103: 0.0,
    104: 0.0,
    105: 0.0,
    106: 0.0
    },
    0)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                            M A I N                                        #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
if not os.path.exists(soludir):
    os.makedirs(soludir)
os.chdir(soludir)


for i in range(0,int(maxStep/deltaStep)+1):
    step=i*deltaStep
    print("#################################################################################################")
    print("#################################### step i = : ",step , "#######################################")
    print("#################################################################################################")
    # current
    
    windingconfig = {0:                                  # phase     = 0
                        {3 :                             # material label 3 
                        {131 : pygeo.GeoVec(0, 0., j_left[step]),   # nodeLabel = 131, current flows out
                        132 : pygeo.GeoVec(0.,0., -j_left[step])}     # nodeLabel = 132, current flows in
                        ,
                        4 :                               # coilLabel = 4, elem label
                        {141 : pygeo.GeoVec(0, 0., j_right[step]),   # nodeLabel = 141, current flows out
                        142 : pygeo.GeoVec(0.,0., -j_right[step])} # nodeLabel = 142, current flows in
                    },
                    }

# Calculate the T-potential for the current distribution of each phase
    windingCurrentMap = generic_excitationcurrent(windingconfig)

    
    reluctivity_matrix.settimestep(i)
    

    # The FEM problem
    femprob = MagnetoStatic3D(fm, boundaries.get() , b'CG', b'CHOLESKY', order)
    femprob.j  = zeroField
    femprob.br = br

    # Set excitations
    # The electric vector potential T and its corresponding tuple of labels are added to the femprob
    femprob.setElectricVectorpotential(windingCurrentMap[phase][0], windingCurrentMap[phase][1])
    
    if step==0:#NullSoluV = femProb.buildNullSoluV(); 
        pass#NullSoluA = femProb.buildNullSoluA();#femProb.setOldSolutions(NullSoluA,NullSoluV); # NullSoluV gibt es auch nicht
    else: #femProb.setOldSolutionsPy(oldSoluA)#, oldSoluV) # setOldSolutionsPy gibt es so auch nicht, muesste aber prinzipiell nix anderes machen als femprob.previousSolution = oldSoluA
        femprob.getEquationSystem().provideSolution(soluAPetsc)
    


    # Set material properties
    femprob.nu = reluctivity
    # Set for hysteresis
    reluctivity_matrix.settimestep(step)
    femprob.setAnisotropicLabels(tensorlabels)
    femprob.setHystereticLabels(tensorlabels)
    femprob.nu_matrix= reluctivity_matrix
    femprob.nu_matrix.__deltaT=deltaT
    femprob.previous_nu = femprob.nu
    femprob.previous_num=femprob.nu_matrix
    femprob.solveTensor = True
    femprob.solveTransient = False
    hr.setResultAttr(femprob.nu)    
    femprob.previous_h=hr
    femprob.prepareMaterialPropertiesTimeStep()
    # Initiation of iteration scheme
    #nrIteration = iterationschemes.NewtonRaphsonIteration(femprob)
    #nrIteration = Newton(femprob,NewtonRelax = True, dimension=3, maxNLIterations=120, timestep=i*deltaT,cGTolerance = 1e-7, maxCGIterations = 1e4,nLTolerance = 5e-4,aTolerance = 1e-10,soltype='indirect', residuumFileName='Residuum.txt')

    #nrIteration = Newton(femprob,NewtonRelax = True, dimension=3, maxNLIterations=50, timestep=step*deltaT,cGTolerance = 1e-7, maxCGIterations = int(3e4),nLTolerance = 5e-4,aTolerance = 1e-7,soltype='indirect', residuumFileName='Residuum.txt')
    nrIteration = Newton(femprob,hyst_mod, NewtonRelax = True, dimension=3, maxNLIterations=50, timestep=step*deltaT,cGTolerance = 1e-7, maxCGIterations = int(5e4),nLTolerance = 5e-4,aTolerance = 1e-7,soltype='indirect', residuumFileName='Residuum.txt')


    # Set tolerances of solver
    #nrIteration.cGTolerance = 1e-7
    #nrIteration.nLTolerance = 1e-4
    #nrIteration.relativeTolerance = 5e-4

    # Solve the problem
    nrIteration.solveNonLinearSystem(priorLinear=False)
    #raise
    # Write flux density
    aPostEdge = pypost.PostEdgeDouble(fm)                   # create object for fem solution of A-potential
    aPostEdge.fromFemSolu(femprob.getEdgeSolution(), order) # write fem solution
    soluA = femprob.getEdgeSolution()
    #soluAPetsc = femprob.getEquationSystem().getXvec()*1.0 ; femprob.getEquationSystem().provideSolution(soluAPetsc);
    #femprob.provideSolution(const EdgeSoluAsso<double>&);

    #oldSoluA = soluA*1.0
    soluAPetsc = femprob.getEquationSystem().getXvec()*1.0 ; 
    
    sfh.writeSolu(soluName+"_A"'.%04d' % i, soluA)
    bPostEdge = aPostEdge.curl()                            # B = curl(A)
    sfh.writeSoluMidValue(soluName+"_B"'.%04d' % i, bPostEdge)              # write solution
    fieldstrength=femprob.nu.calcHpost()+femprob.nu_matrix.calcHpost() #due to two seperate classes unfortunately field has to be superposed
    sfh.writeSoluMidValue(soluName+"_H"'.%04d' % i, fieldstrength)

    (value_S)=femprob.nu_matrix.calcRSprevTime()    # 
    reluctivity_matrix.setStopPreviousRS(value_S)


    time=step*deltaT


    ind = pypost.PostVecDouble(fm)
    sfh.readSoluMidValue(soluName+"_B"'.%04d' % i,ind)
    absBPVx = ind.xComponent()
    absBPVy = ind.yComponent()
    sfh.readSoluMidValue(soluName+"_H"'.%04d' % i,ind)
    absHPVx = ind.xComponent()
    absHPVy = ind.yComponent()

    pointsx = open("pointsx",'a')
    pointsy = open("pointsy",'a')
    
    time=i*deltaT
    pointsx.write(str(i)+'\t'+str(time)+'\t'+str(j_left[i*deltaStep])+'\t'+str(absHPVx.valueAtCoord(P1))+'\t'+str(absBPVx.valueAtCoord(P1))+'\t'+str(absHPVx.valueAtCoord(P2))+'\t'+str(absBPVx.valueAtCoord(P2))+'\t''\n')
    pointsy.write(str(i)+'\t'+str(time)+'\t'+str(j_right[i*deltaStep])+'\t'+str(absHPVy.valueAtCoord(P1))+'\t'+str(absBPVy.valueAtCoord(P1))+'\t'+str(absHPVy.valueAtCoord(P2))+'\t'+str(absBPVy.valueAtCoord(P2))+'\t''\n')
    pointsx.close() # Open file for writing
    pointsy.close()


    del femprob

